﻿using apiWorkers.BusinessLogic;
using apiWorkers.BusinessLogic.JsonParser;
using apiWorkers.BusinessLogic.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace apiWorkers.Controllers
{
    //api/calcSalary
    public class calcSalaryController : ApiController
    {
        [HttpPost]
        public async System.Threading.Tasks.Task<String> PostRawBufferManual()
        {
            //тело запроса json обрабатывается здесь
            string jsonRequest = await Request.Content.ReadAsStringAsync();
            JsonParser parser = new JsonParser();
            string salary = "";
            
            try
            {
                ListOfWorkers workers = parser.getObjects(jsonRequest);
                salary = workers.getSalary().ToString();
            }
            catch (CustomException ex)
            {
                return ("Exception :" + ex.ToString());
            }
            return salary;
        }
    }
}
