﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace apiWorkers.BusinessLogic.Workers
{   
    //интерфейс для расчета получаемых денег сотрудником
    interface IEarnings
    {
        float calcEarnings();
    }
}
