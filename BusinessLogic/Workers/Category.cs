﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiWorkers.BusinessLogic.Workers
{
    //класс для преобразования категории в коэфициент приращения зарплаты
    public static class Category
    {
        private const float A = 1.25f;
        private const float B = 1.15f;
        private const float C = 1.00f;
        //функция возвращает коэффициент на который нужно домножить, чтобы получить ЗП
        public static float getValue(string _koeficient)
        {
            float result = C;
            switch (_koeficient)
            {
                case "A":
                    result = A;
                    break;
                case "B":
                    result = B;
                    break;
                case "C":
                    result = C;
                    break;
                default:
                    //если категорию не нашли, то необходимо выдать предупреждение об этом
                    throw new CustomException("Uncknown category in json request: " + _koeficient);
                    break;
            }
            return result;
        }
        //Функция проверяет есть ли такая категория
        public static bool checkCategory(string _koeficient)
        {
            bool result = false;
            switch (_koeficient)
            {
                case "A":
                case "B":
                case "C":
                    result = true;
                    break;
                default:
                    //если категорию не нашли, то необходимо выдать предупреждение об этом
                    Console.WriteLine("Uncknown category in checkCategory(): " + _koeficient);
                    break;
            }
            return result;
        }
    }
}