﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiWorkers.BusinessLogic.Workers
{
    //данный класс считает зарплату всех работников в списке
    public class ListOfWorkers
    {
        private List<Worker> workers;

        public Worker getFirst()
        {
            return workers.First();
        }

        public ListOfWorkers()
        {
            workers = new List<Worker>();
        }

        public void addWorker(Worker _worker)
        {
            workers.Add(_worker);
        }

        public float getSalary()
        {
            float allSalary = 0;
            foreach (Worker worker in workers)
            {
                allSalary += worker.calcEarnings();
            }
            return allSalary;
        }
    }
}