﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiWorkers.BusinessLogic.Workers
{
    //базовый класс, в тех. задание написано, что каждый сотрудник получает зарплату плюс бонус
    public class Worker : IEarnings
    {
        public string position { get; set; }
        public float salary { get; set; }
        public float bonus { get; set; }

        public Worker(string _position, float _salary, float _bonus)
        {
            position = _position;
            salary = _salary;
            bonus = _bonus;
        }

        public virtual float calcEarnings()
        {
            return bonus + salary;
        }
    }
}