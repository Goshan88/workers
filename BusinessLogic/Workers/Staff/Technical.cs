﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiWorkers.BusinessLogic.Workers.Staff
{
    //Техники в дополнении к окладу и бонусу добавляется категория
    public class Technical : Worker
    {
        public string category { get; set; }
        public Technical(string _position, float _salary, float _bonus, string _category)
            : base(_position, _salary, _bonus)
        {
            category = _category;
        }

        public override float calcEarnings()
        {
            return (bonus + salary * Category.getValue(category));
        }    
    }
}