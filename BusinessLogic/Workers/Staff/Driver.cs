﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiWorkers.BusinessLogic.Workers.Staff
{
    //так как в задание стояло условие что код должен быть минимален то наследование идет от техника
    //водители получают зарплату взависимости от отработанных часов и категории
    public class Driver : Technical
    {
        public int timeWorked { get; set; }
        public Driver(string _position, float _salary, float _bonus, string _category, int _timeWorked)
            : base(_position, _salary, _bonus, _category)
        {
            timeWorked = _timeWorked;
        }

        public override float calcEarnings()
        {
            return (bonus + salary * timeWorked * Category.getValue(category));
        }
    }
}