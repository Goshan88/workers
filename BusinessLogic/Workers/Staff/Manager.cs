﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiWorkers.BusinessLogic.Workers.Staff
{
    //Менеджер имеет только оклад и бонусы, самый простой класс
    public class Manager : Worker
    {
        public Manager(string _position, float _salary, float _bonus)
        :base(_position,_salary,_bonus) { }
    }
}