﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using apiWorkers;
using apiWorkers.Controllers;
using apiWorkers.BusinessLogic.JsonParser;
using apiWorkers.BusinessLogic.Workers;
*/
/*Visual Studio создала проект для тестирования отдельно
 * для того чтобы представить тесты в данном проекте, я
 * копировал автотесты в этот документ
 */
 /*

namespace apiWorkers.Tests.Controllers
{
    [TestClass]
    public class calcSalaryControllerTest
    {

        [TestMethod]
        public void parsingManager()
        {
            // Arrange
            JsonParser parser = new JsonParser();

            // Act
            ListOfWorkers workers = parser.getObjects("{\"position\":\"manager\",\"salary\": \"150000\",\"bonus\": \"0\"}");
            Worker tmp = workers.getFirst();

            // Assert
            Assert.AreEqual("manager", tmp.position);
            Assert.AreEqual(150000.0f, tmp.salary);
            Assert.AreEqual(0.0f, tmp.bonus);
        }

        [TestMethod]
        public void getSalaryManager()
        {
            // Arrange
            JsonParser parser = new JsonParser();

            // Act
            ListOfWorkers workers = parser.getObjects("{\"position\":\"manager\",\"salary\": \"150000\",\"bonus\": \"0\"}");

            // Assert
            Assert.AreEqual(150000.0f, workers.getSalary());
        }

        [TestMethod]
        public void parsingTechnical()
        {
            // Arrange
            JsonParser parser = new JsonParser();

            // Act
            ListOfWorkers workers = parser.getObjects("{\"position\":\"technician\",\"salary\": \"35000\",\"bonus\": \"5000\",\"category\": \"B\"}");
            Worker tmp = workers.getFirst();

            // Assert
            Assert.AreEqual("technician", tmp.position);
            Assert.AreEqual(35000.0f, tmp.salary);
            Assert.AreEqual(5000.0f, tmp.bonus);
        }


        [TestMethod]
        public void getSalaryTechnical()
        {
            // Arrange
            JsonParser parser = new JsonParser();

            // Act
            ListOfWorkers workers = parser.getObjects("{\"position\":\"technician\",\"salary\": \"35000\",\"bonus\": \"5000\",\"category\": \"B\"}");

            // Assert
            Assert.AreEqual(45250.0f, workers.getSalary());
        }

        [TestMethod]
        public void parsingDriver()
        {
            // Arrange
            JsonParser parser = new JsonParser();

            // Act
            ListOfWorkers workers = parser.getObjects("{\"position\":\"driver\",\"salary\": \"250\",\"bonus\": \"10000\",\"timeWorked\": \"80\",\"category\": \"B\"}");
            Worker tmp = workers.getFirst();

            // Assert
            Assert.AreEqual("driver", tmp.position);
            Assert.AreEqual(250.0f, tmp.salary);
            Assert.AreEqual(10000.0f, tmp.bonus);
        }


        [TestMethod]
        public void getSalaryDriver()
        {
            // Arrange
            JsonParser parser = new JsonParser();

            // Act
            ListOfWorkers workers = parser.getObjects("{\"position\":\"driver\",\"salary\": \"250\",\"bonus\": \"10000\",\"timeWorked\": \"80\",\"category\": \"B\"}");

            // Assert
            Assert.AreEqual(33000.0f, workers.getSalary());
        }


        [TestMethod]
        public void getSalaryFromArray()
        {
            // Arrange
            JsonParser parser = new JsonParser();

            // Act
            ListOfWorkers workers = parser.getObjects("[{\"position\":\"manager\",\"salary\": \"150000\",\"bonus\": \"0\"}, {\"position\":\"technician\",\"salary\": \"35000\",\"bonus\": \"5000\",\"category\": \"B\"},{\"position\":\"driver\",\"salary\": \"250\",\"bonus\": \"10000\",\"timeWorked\": \"80\",\"category\": \"B\"}]");

            // Assert
            Assert.AreEqual(228250.0f, workers.getSalary());
        }

    }
}
*/