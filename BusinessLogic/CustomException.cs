﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiWorkers.BusinessLogic
{
    //Для обработки исключений:
    //              ошибка в схеме json,
    //              отсутствует должность из запроса
    //              отсутствует категория в запросе
    public class CustomException:Exception
    {
        public CustomException(string message)
            : base(message) 
        { }
    }
}