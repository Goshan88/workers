﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using apiWorkers.BusinessLogic.Workers;
using apiWorkers.BusinessLogic.Workers.Staff;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace apiWorkers.BusinessLogic.JsonParser
{
    /*класс создан для преобразование json запрса в объекты*/
    public class JsonParser
    {
        public ListOfWorkers getObjects(string json)
        {
            ListOfWorkers workers = new ListOfWorkers();
            Boolean jsonIsArray = false;
            //проверяем если запрос состоит только из одного сотрудника
            //или из массива сотрудников
            try
            {
                var token = JToken.Parse(json);
                jsonIsArray = (token is JArray);
            }
            catch (JsonException ex)
            {
                throw new CustomException("Exception in scheme json" + ex.ToString());
            }

            if (jsonIsArray)
            {
                dynamic dynObj = null;
                dynObj = JsonConvert.DeserializeObject(json);
                foreach (var worker in dynObj)
                {
                    workers.addWorker(getObject(worker.ToString()));
                }
            }
            else{
                workers.addWorker(getObject(json));
            }
            /*
            try
            {
                JObject data = JObject.Parse(json);
                workers.addWorker(getObject(data.ToString()));
            }
            catch (JsonReaderException ex)
            {
                //объект
            }

            try
            {
                dynObj = JsonConvert.DeserializeObject(json);
            }
            catch(JsonException ex)
            {
                throw new CustomException("Exception in scheme json");
            }

           
            foreach (var person in dynObj)
            {
                
            }*/

            return workers;
        }


        private Worker getObject(string _person)
        {
            Worker tmpPerson = JsonConvert.DeserializeObject<Worker>(_person);
            if (tmpPerson.position == "manager")
            {
                return tmpPerson;
            }
            else if (tmpPerson.position == "technician")
            {
                Technical tmpTechnick = JsonConvert.DeserializeObject<Technical>(_person);
                return tmpTechnick;
            }
            else if (tmpPerson.position == "driver")
            {
                Driver tmpDriver = JsonConvert.DeserializeObject<Driver>(_person);
                return tmpDriver;
            }
            else
            {//неизвестная профессия
                throw new CustomException("Unknown position in json request: " + tmpPerson.position);
            }
        }
    }
}